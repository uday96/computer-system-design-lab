import java.io.*;
import java.util.*;

public class Ass4{

	public static String intTo16BitStr(int n){
		String bin = Integer.toBinaryString(n);
		if(bin.length() > 16){
			bin = bin.substring(bin.length() - 16);
		}
		else{
			while(bin.length()<16){
				bin = "0"+bin;
			}
		}
		////System.out.println(bin);
		return bin;
	}
	
	public static Map<String, String> regCodes = new HashMap<String, String>();
	public static Map<String, String> opCodes = new HashMap<String, String>();
	public static Map<String, Integer> regVals = new HashMap<String, Integer>();
	public static Map<String, String> memoryVals = new HashMap<String, String>();
	public static Map<String, Integer> labelOffsets = new HashMap<String, Integer>();
	public static HashMap<String, ArrayList<Integer>> regDependencies = new HashMap<String, ArrayList<Integer>>();
	public static HashMap<String, ArrayList<Integer>> regForwarding = new HashMap<String, ArrayList<Integer>>();

	public static InstructionState IF(int PC){
		String instr = memoryVals.get(String.format("%04d", PC));
		instr += " "+memoryVals.get(String.format("%04d", PC+1));
		int NAR = PC+2;
		InstructionState InstrStateObj = new InstructionState(instr,NAR);
		return InstrStateObj;
	}

	public static InstructionState DE(InstructionState InstrStateObj){
		String[] instrParts = InstrStateObj.instruction.split("\\s");
		String B1 = instrParts[3];
		String B2 = instrParts[2];
		String IMM = instrParts[0]+" "+instrParts[1];
		InstrStateObj.B1 = B1;
		InstrStateObj.B2 = B2;
		InstrStateObj.IMM = IMM;

		if(instrParts[0].equals("1110")){ //HLT
			InstrStateObj.isHLT = true;
			return InstrStateObj;
		}

		boolean isRAW = false;
		if(instrParts[0].equals("1101")){ // BEQZ
			isRAW = checkRAWDependencies(instrParts[1]);
		}
		else if(instrParts[0].equals("0000") || instrParts[0].equals("0010") || instrParts[0].equals("0110")){
			isRAW = checkRAWDependencies(B1) || checkRAWDependencies(B2);
		}
		else if(instrParts[0].equals("0001") || instrParts[0].equals("0011") || instrParts[0].equals("0111")){
			isRAW = checkRAWDependencies(B2);
		}
		else if(instrParts[0].equals("1000")){ // Load
			isRAW = checkRAWDependencies(B1) || checkRAWDependencies(B2);	
		}
		else if(instrParts[0].equals("1010")){ // Store
			isRAW = checkRAWDependencies(instrParts[1]) || checkRAWDependencies(B1) || checkRAWDependencies(B2);
		}

		InstrStateObj.isRAW = isRAW;

		if(isRAW){
			return InstrStateObj;
		}

		if(instrParts[0].equals("1100") || instrParts[0].equals("1101")){
			InstrStateObj.isJMP = true;
		}
		if(instrParts[0].equals("0000") || instrParts[0].equals("0010") || instrParts[0].equals("0110")){
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(0);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}
		if(instrParts[0].equals("0001") || instrParts[0].equals("0011") || instrParts[0].equals("0111")){
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(0);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}
		if(instrParts[0].equals("1000")){ // Load
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(0);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}
		return InstrStateObj;
	}

	public static InstructionState EX(InstructionState InstrStateObj){
		String[] instrSubParts = InstrStateObj.IMM.split("\\s");
		String B1 = InstrStateObj.B1;
		String B2 = InstrStateObj.B2;
		String Operation = instrSubParts[0];
		String Reg = instrSubParts[1];
		int B3 = -1;
		if(Operation.equals("0000") || Operation.equals("0010") || Operation.equals("0110")){
			int regVal1;
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = getRegWithFwd(B1);
			if(Operation.equals("0000")){
				regVal1 = regVal2 + regVal3;
			}
			else if(Operation.equals("0010")){
				regVal1 = regVal2 - regVal3;
			}
			else{
				regVal1 = regVal2 * regVal3;
			}
			B3 = regVal1;
			ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			RegFwd.add(B3);
			regForwarding.put(Reg,RegFwd);
			//System.out.println("RegFwd Added - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("0001") || Operation.equals("0011") || Operation.equals("0111")){
			int regVal1;
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = Integer.parseInt(B1,2);
			if(regVal3>=8){
				regVal3-=16;
			}
			if(Operation.equals("0001")){
				regVal1 = regVal2 + regVal3;
			}
			else if(Operation.equals("0011")){
				regVal1 = regVal2 - regVal3;
			}
			else{
				regVal1 = regVal2 * regVal3;
			}
			B3 = regVal1;
			ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			RegFwd.add(B3);
			regForwarding.put(Reg,RegFwd);
			//System.out.println("RegFwd Added - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("1000")){ //Load
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = getRegWithFwd(B1);
			int mem = regVal2+regVal3;
			B3 = mem;
		}
		else if(Operation.equals("1010")){ //Store
			int mem = getRegWithFwd(instrSubParts[1])+getRegWithFwd(B2);
			B3 = mem;
		}
		InstrStateObj.B3 = B3;
		InstrStateObj.Reg = Reg;
		InstrStateObj.Operation = Operation;
		return InstrStateObj;
	}

	public static InstructionState MEM(InstructionState InstrStateObj){
		String Operation = InstrStateObj.Operation;
		int DMAR = InstrStateObj.B3;
		int NAR = InstrStateObj.NAR;
		int MDR = -1;
		int PC = NAR;
		if(Operation.equals("1000")){ //Load
			String memVal = memoryVals.get(String.format("%04d", (DMAR)))+" "+memoryVals.get(String.format("%04d", (DMAR+1)));
			memVal = memVal.replace(" ","");
			MDR = Integer.parseInt(memVal,2);
			if(MDR>=32768){
				MDR = MDR - 65536;
			}
		}
		else if(Operation.equals("1010")){ //Store
			MDR = regVals.get(InstrStateObj.B1);
			String binMDR = intTo16BitStr(MDR);
			memoryVals.put(String.format("%04d", DMAR), binMDR.substring(0,4)+" "+binMDR.substring(4,8));
			memoryVals.put(String.format("%04d", DMAR+1), binMDR.substring(8,12)+" "+binMDR.substring(12,16));
		}
		else if(Operation.equals("1100")){ // JMP
			String offBin = InstrStateObj.Reg + InstrStateObj.B2;
			int off = Integer.parseInt(offBin,2);
			if(off>=128){
                off-=256;
            }
            PC = NAR-2+off;
		}
		else if(Operation.equals("1101")){ // BEQZ
            int regVal = regVals.get(InstrStateObj.Reg);
            if(regVal == 0){
            	String offBin = InstrStateObj.B2 + InstrStateObj.B1;
				int off = Integer.parseInt(offBin,2);
				if(off>=128){
                    off-=256;
                }
                PC = NAR-2+off;
            }
		}
		InstrStateObj.MDR = MDR;
		InstrStateObj.NPC = PC;
		return InstrStateObj;
	}

	public static InstructionState SR(InstructionState InstrStateObj){
		String Operation = InstrStateObj.Operation;
		int B3 = InstrStateObj.B3;
		int MDR = InstrStateObj.MDR;
		String Reg = InstrStateObj.Reg;
		if(Operation.equals("0000") || Operation.equals("0010") || Operation.equals("0110") ||
			Operation.equals("0001") || Operation.equals("0011") || Operation.equals("0111")){
			regVals.put(Reg,B3);

			ArrayList<Integer> RegDep = regDependencies.get(Reg);
			RegDep.remove(0);
			regDependencies.put(Reg,RegDep);
			//System.out.println("Removed Dep - "+Reg+" - "+RegDep.toString());

			ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			RegFwd.remove(0);
			regForwarding.put(Reg,RegFwd);
			//System.out.println("Removed Fwd - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("1000")){ //Load
			regVals.put(Reg,MDR);

			ArrayList<Integer> RegDep = regDependencies.get(Reg);
			RegDep.remove(0);
			regDependencies.put(Reg,RegDep);
		}
		//System.out.println("SR Fin - "+InstrStateObj.NAR);
		return InstrStateObj;
	}

	public static boolean checkRAWDependencies(String reg){
		ArrayList<Integer> regUsage = regDependencies.get(reg);
		//System.out.println(reg+" - "+regUsage.toString());
		if(regUsage.size() == 0){
			return false;
		}
		boolean isDep = regUsage.contains(0);
		if(isDep){
			ArrayList<Integer> regFwd = regForwarding.get(reg);
			if(regFwd.size() != 0){
				return false;
			} 
		}
		return isDep;
	}

	public static int getRegWithFwd(String reg){
		ArrayList<Integer> regFwd = regForwarding.get(reg);
		if(regFwd.size() > 0){
			return regFwd.get(0);
		}
		return regVals.get(reg);
	}

	public static void main(String[] args) throws IOException {

		for(int n=0;n<16;n++){
			String code = Integer.toBinaryString(n);
			while(code.length()<4){
				code = "0"+code;
			}
			regCodes.put("R"+Integer.toString(n),code);

			ArrayList<Integer> depCodes = new ArrayList<Integer>();
			regDependencies.put(code,depCodes);
			ArrayList<Integer> fwdVals = new ArrayList<Integer>();
			regForwarding.put(code,fwdVals);
		}

		opCodes.put("ADD" ,"0000");
		opCodes.put("SUB" ,"0010");
		opCodes.put("MUL" ,"0110");
		opCodes.put("LD"  ,"1000");
		opCodes.put("SD"  ,"1010");
		opCodes.put("JMP" ,"1100");
		opCodes.put("BEQZ","1101");
		opCodes.put("HLT" ,"1110");
		opCodes.put("ADDIM" ,"0001");
		opCodes.put("SUBIM" ,"0011");
		opCodes.put("MULIM" ,"0111");
		
		for(int i = 0; i < 16; i++) {
			regVals.put(regCodes.get("R"+Integer.toString(i)), 0);
		}
		
		for(int i = 0; i < 1024; i++) {
			memoryVals.put(String.format("%04d", i), "0000 0000");
		}

		PrintWriter writer = new PrintWriter("input.txt", "UTF-8");
		Scanner scan = new Scanner(System.in);
		String line = scan.nextLine();
		while(!(line.contains("HLT"))){
			writer.println(line);
			line = scan.nextLine();
		}
		writer.println(line);
		writer.close();

		File file = new File("input.txt");
		BufferedReader b = new BufferedReader(new FileReader(file));
		line=null;
		int hashes = 0;
		int instrNum = 0;
		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			if(hashes == 3){
				if(line.contains(":")){
					String label = (line.split(":"))[0];
					labelOffsets.put(label,instrNum);
				}
				instrNum++;
			}
		}

		b = new BufferedReader(new FileReader(file));
		line = null;
		hashes = 0;
		int PC=0;
		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			if(hashes == 1){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					System.out.println("Invalid Input");
					System.exit(0);
				}
				regVals.put(regCodes.get(parts[1].trim()),Integer.parseInt(parts[2].trim()));
			}
			else if(hashes == 2){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					System.out.println("Invalid Input");
					System.exit(0);
				}
				int mem = Integer.parseInt(parts[1].trim());
				String binVal = intTo16BitStr(Integer.parseInt(parts[2].trim()));
				memoryVals.put(String.format("%04d", mem), binVal.substring(0,4)+" "+binVal.substring(4,8));
				memoryVals.put(String.format("%04d", mem+1), binVal.substring(8,12)+" "+binVal.substring(12,16));
			}
			else{
				//System.out.println(PC+" - "+line);
				String[] parts = line.split("\\s");
				for(int k=0;k<parts.length;k++){
					parts[k] = parts[k].trim();
				}
				if(parts[0].contains(":")){
					List<String> list = new ArrayList<String>(Arrays.asList(parts));
					list.removeAll(Arrays.asList(parts[0]));
					parts = list.toArray(new String[list.size()]);
				}
				if(parts[0].equals("ADD") || parts[0].equals("SUB") || parts[0].equals("MUL")){
					String binInstr;
					if(parts[3].contains("#")){
						parts[0]+="IM";
					}
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					if(parts[3].contains("#")){
						String bin = Integer.toBinaryString(Integer.parseInt(parts[3].replace("#","")));
						if(bin.length()>4){
							bin = bin.substring(bin.length()-4);
						}
						else{
							while(bin.length()<4){
								bin = "0"+bin;
							}
						}
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+bin);
					}
					else{
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+regCodes.get(parts[3]));
					}
				}
				else if(parts[0].equals("LD")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					String reg1 = (parts[2].split("\\["))[0];
					String reg2 = (((parts[2].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg1)+" "+regCodes.get(reg2));
				}
				else if(parts[0].equals("SD")){
					String reg1 = (parts[1].split("\\["))[0];
					String reg2 = (((parts[1].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg1));
					PC++;
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg2)+" "+regCodes.get(parts[2]));
				}
				else if(parts[0].equals("BEQZ")){
					int offset = labelOffsets.get(parts[2]);
					offset=(2*offset)-PC;
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					String reg = (parts[1].replace("(","")).replace(")","");
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(0,4)+" "+offsetBin.substring(4,8));
				}
				else if(parts[0].equals("JMP")){
					int offset = labelOffsets.get(parts[1]);
					offset=(2*offset)-PC;
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+offsetBin.substring(0,4));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(4,8)+" 0000");
				}
				else if(parts[0].equals("HLT")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" 0000");
                    PC++;
				}
				PC++;
			}
		}
		//System.out.println();
		
		HashMap<String,InstructionState> Pipieline = new HashMap<String,InstructionState>();

		boolean stallPipeline = false;
		boolean isRAWPipeline = false;

		PC = 0;

		while(PC < 512){

			InstructionState NewInstrFetch = IF(PC);
			InstructionState IFOutput = null;
			InstructionState DEOutput = null;
			InstructionState EXOutput = null;
			InstructionState MEMOutput = null;
			InstructionState SROutput = null;
			
			if(Pipieline.containsKey("IF")){
				IFOutput = Pipieline.get("IF");
			}
			if(Pipieline.containsKey("DE")){
				DEOutput = Pipieline.get("DE");
				stallPipeline = DEOutput.isJMP;
				isRAWPipeline = DEOutput.isRAW;
				// if(isRAWPipeline){
				// 	System.out.println("isRAW - "+PC);
				// }
				// if(stallPipeline){
				// 	System.out.println("isJMP - "+PC);
				// }
			}
			if(Pipieline.containsKey("EX")){
				EXOutput = Pipieline.get("EX");
			}
			if(Pipieline.containsKey("MEM")){
				MEMOutput = Pipieline.get("MEM");
				if(MEMOutput.isHLT){
					break;
				}
			}
			if(Pipieline.containsKey("SR")){
				SROutput = Pipieline.get("SR");
			}

			if(DEOutput != null){
				if(DEOutput.isRAW){
					if(MEMOutput != null){
						Pipieline.put("SR",SR(MEMOutput));
					}
					else{
						Pipieline.remove("SR");
					}
					if(EXOutput != null){
						Pipieline.put("MEM",MEM(EXOutput));
					}
					else{
						Pipieline.remove("MEM");
					}
					Pipieline.remove("EX");
					Pipieline.put("DE",DE(DEOutput));
					continue;
				}
				if(DEOutput.isJMP){
					if(MEMOutput != null){
						Pipieline.put("SR",SR(MEMOutput));	
					}
					else{
						Pipieline.remove("SR");
					}
					if(EXOutput != null){
						Pipieline.put("MEM",MEM(EXOutput));	
					}
					else{
						Pipieline.remove("MEM");
					}
					Pipieline.put("EX",EX(DEOutput));
					Pipieline.remove("DE");
					Pipieline.remove("IF");
					continue;
				}
			}
			if(EXOutput != null){
				if(EXOutput.isJMP){
					if(MEMOutput != null){
						Pipieline.put("SR",SR(MEMOutput));	
					}
					else{
						Pipieline.remove("SR");
					}
					Pipieline.put("MEM",MEM(EXOutput));
					Pipieline.remove("EX");
					continue;
				}	
			}
			if(MEMOutput != null){
				if(MEMOutput.isJMP){
					Pipieline.remove("SR");
					Pipieline.remove("MEM");
					PC = MEMOutput.NPC;
					stallPipeline = false;
					//System.out.println("-----------"+PC);
					continue;
				}	
			}

			if(MEMOutput != null){
				Pipieline.put("SR",SR(MEMOutput));	
			}
			else{
				Pipieline.remove("SR");
			}

			if(EXOutput != null){
				Pipieline.put("MEM",MEM(EXOutput));
			}
			else{
				Pipieline.remove("MEM");
			}

			if(DEOutput != null){
				Pipieline.put("EX",EX(DEOutput));
			}
			else{
				Pipieline.remove("EX");
			}

			if(IFOutput != null){
				Pipieline.put("DE",DE(IFOutput));
				//System.out.println("DE Fin - "+PC);
			}
			else{
				Pipieline.remove("DE");
			}

			if(NewInstrFetch != null){
				Pipieline.put("IF",NewInstrFetch);
			}
			else{
				Pipieline.remove("IF");
			}

			PC+=2;
		}
		
		writer = new PrintWriter("output.txt", "UTF-8");
		for (int i = 0; i < 1024; i++){
			writer.println(String.format("%04d", i) +" : "+ memoryVals.get(String.format("%04d", i)));
		}
		writer.close();
		file.delete();
	}
}

class InstructionState {
	
	String instruction;
	int NAR;

	String B1;
	String B2;
	String IMM;

	int B3;
	String Reg;
	String Operation;

	int MDR;
	int NPC;

	boolean isJMP;
	boolean isRAW;
	boolean isHLT;

	InstructionState(String instruction, int NAR){
		this.instruction = instruction;
		this.NAR = NAR;
		this.isJMP = false;
		this.isRAW = false;
		this.isHLT = false;
	}
}