import java.io.*;
import java.util.*;

public class Ass3{

	public static String intTo16BitStr(int n){
		String bin = Integer.toBinaryString(n);
		if(bin.length() > 16){
			bin = bin.substring(bin.length() - 16);
		}
		else{
			while(bin.length()<16){
				bin = "0"+bin;
			}
		}
		////System.out.println(bin);
		return bin;
	}
	
	public static void main(String[] args) throws IOException 
	{
		Map<String, String> regCodes = new HashMap<String, String>();
		Map<String, String> opCodes = new HashMap<String, String>();

		for(int n=0;n<16;n++){
			String code = Integer.toBinaryString(n);
			while(code.length()<4){
				code = "0"+code;
			}
			regCodes.put("R"+Integer.toString(n),code) ;
			////System.out.println("R"+Integer.toString(n)+" : "+code);
		}

		opCodes.put("ADD" ,"0000");
		opCodes.put("SUB" ,"0010");
		opCodes.put("MUL" ,"0110");
		opCodes.put("LD"  ,"1000");
		opCodes.put("SD"  ,"1010");
		opCodes.put("JMP" ,"1100");
		opCodes.put("BEQZ","1101");
		opCodes.put("HLT" ,"1110");
		opCodes.put("ADDIM" ,"0001");
		opCodes.put("SUBIM" ,"0011");
		opCodes.put("MULIM" ,"0111");
		
		Map<String, Integer> regVals = new HashMap<String, Integer>();
		Map<String, String> memoryVals = new HashMap<String, String>();
		Map<String, Integer> labelOffsets = new HashMap<String, Integer>();
		
		for(int i = 0; i < 16; i++) {
			regVals.put(regCodes.get("R"+Integer.toString(i)), 0);
		}
		
		for(int i = 0; i < 1024; i++) {
			memoryVals.put(String.format("%04d", i), "0000 0000");
		}

		PrintWriter writer = new PrintWriter("input.txt", "UTF-8");
		Scanner scan = new Scanner(System.in);
		String line = scan.nextLine();
		while(!(line.contains("HLT"))){
			writer.println(line);
			line = scan.nextLine();
		}
		writer.println(line);
		writer.close();

		File file = new File("input.txt");
		BufferedReader b = new BufferedReader(new FileReader(file));
		line=null;
		int hashes = 0;
		int instrNum = 0;
		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			if(hashes == 3){
				if(line.contains(":")){
					String label = (line.split(":"))[0];
					labelOffsets.put(label,instrNum);
					//System.out.println(label+" : "+instrNum);
				}
				instrNum++;
			}
		}

		b = new BufferedReader(new FileReader(file));
		line = null;
		hashes = 0;
		int PC=0;
		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			//System.out.println(line);
			if(hashes == 1){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					//System.out.println("Invalid Input");
					System.exit(0);
				}
				regVals.put(regCodes.get(parts[1].trim()),Integer.parseInt(parts[2].trim()));
				//System.out.println(regCodes.get(parts[1].trim())+" : "+Integer.parseInt(parts[2].trim()));
			}
			else if(hashes == 2){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					//System.out.println("Invalid Input");
					System.exit(0);
				}
				int mem = Integer.parseInt(parts[1].trim());
				String binVal = intTo16BitStr(Integer.parseInt(parts[2].trim()));
				memoryVals.put(String.format("%04d", mem), binVal.substring(0,4)+" "+binVal.substring(4,8));
				//System.out.println(String.format("%04d", mem)+" : "+ binVal.substring(0,4)+" "+binVal.substring(4,8));
				memoryVals.put(String.format("%04d", mem+1), binVal.substring(8,12)+" "+binVal.substring(12,16));
				//System.out.println(String.format("%04d", mem+1)+" : "+ binVal.substring(8,12)+" "+binVal.substring(12,16));
			}
			else{
				String[] parts = line.split("\\s");
				for(int k=0;k<parts.length;k++){
					parts[k] = parts[k].trim();
				}
				if(parts[0].contains(":")){
					List<String> list = new ArrayList<String>(Arrays.asList(parts));
					list.removeAll(Arrays.asList(parts[0]));
					parts = list.toArray(new String[list.size()]);
				}
				if(parts[0].equals("ADD") || parts[0].equals("SUB") || parts[0].equals("MUL")){
					String binInstr;
					if(parts[3].contains("#")){
						parts[0]+="IM";
					}
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					if(parts[3].contains("#")){
						String bin = Integer.toBinaryString(Integer.parseInt(parts[3].replace("#","")));
						if(bin.length()>4){
							bin = bin.substring(bin.length()-4);
						}
						else{
							while(bin.length()<4){
								bin = "0"+bin;
							}
						}
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+bin);
						//System.out.println(String.format("%04d", PC)+" : "+ regCodes.get(parts[2])+" "+bin);
					}
					else{
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+regCodes.get(parts[3]));
						//System.out.println(String.format("%04d", PC)+" : "+ regCodes.get(parts[2])+" "+regCodes.get(parts[3]));
					}
				}
				else if(parts[0].equals("LD")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					//System.out.println(parts[2]);
					String reg1 = (parts[2].split("\\["))[0];
					String reg2 = (((parts[2].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg1)+" "+regCodes.get(reg2));
					//System.out.println(String.format("%04d", PC)+" : "+ regCodes.get(reg1)+" "+regCodes.get(reg2));
				}
				else if(parts[0].equals("SD")){
					String reg1 = (parts[1].split("\\["))[0];
					String reg2 = (((parts[1].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg1));
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" "+regCodes.get(reg1));
					PC++;
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg2)+" "+regCodes.get(parts[2]));
					//System.out.println(String.format("%04d", PC)+" : "+ regCodes.get(reg2)+" "+regCodes.get(parts[2]));
				}
				else if(parts[0].equals("BEQZ")){
					int offset = labelOffsets.get(parts[2]);
					offset=(2*offset)-PC;
					//System.out.println(offset);
					//System.out.println(PC);
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					String reg = (parts[1].replace("(","")).replace(")","");
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg));
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" "+regCodes.get(reg));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(0,4)+" "+offsetBin.substring(4,8));
					//System.out.println(String.format("%04d", PC)+" : "+ offsetBin.substring(0,4)+" "+offsetBin.substring(4,8));
				}
				else if(parts[0].equals("JMP")){
					int offset = labelOffsets.get(parts[1]);
					offset=(2*offset)-PC;
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+offsetBin.substring(0,4));
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" "+offsetBin.substring(0,4));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(4,8)+" 0000");
					//System.out.println(String.format("%04d", PC)+" : "+ offsetBin.substring(4,8)+" 0000");
				}
				else if(parts[0].equals("HLT")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" 0000");
					//System.out.println(String.format("%04d", PC)+" : "+ opCodes.get(parts[0])+" 0000");
                    PC++;
				}
				PC++;
			}
		}
		//System.out.println('\n');
		PC = 0;
		while(PC < 512){
			//System.out.println(String.format("%04d", PC));
			String instr = memoryVals.get(String.format("%04d", PC));
			instr += " "+memoryVals.get(String.format("%04d", PC+1));
			String[] instrParts = instr.split("\\s");
			if(instrParts[0].equals("1110")){
				//HLT
				break;
			}
			else if(instrParts[0].equals("1100")){
				// JMP
				String offBin = instrParts[1]+instrParts[2];
				//System.out.println(offBin);
				int off = Integer.parseInt(offBin,2);
				//System.out.println("offbefr: "+off);
				if(off>=128){
                    off-=256;
                }
                PC+=off;
                //System.out.println("offaftr: "+off);
                //System.out.println("pc: "+PC);
                //System.out.println("---------"+String.format("%04d", PC));
			}
			else if(instrParts[0].equals("1101")){
				// BEQZ
                int regVal = regVals.get(instrParts[1]);
                if(regVal == 0){
                	String offBin = instrParts[2]+instrParts[3];
					int off = Integer.parseInt(offBin,2);
					//System.out.println("offbefr: "+off);
					if(off>=128){
	                    off-=256;
	                }
	                PC+=off;
	                //System.out.println("offaftr: "+off);
                	//System.out.println("pc: "+PC);
                }
                else{
                	PC+=2;
                }
                //System.out.println("---------"+String.format("%04d", PC));
			}
			else if(instrParts[0].equals("0000") || instrParts[0].equals("0010") || instrParts[0].equals("0110")){
				int regVal1;
				int regVal2 = regVals.get(instrParts[2]);
				int regVal3 = regVals.get(instrParts[3]);
				if(instrParts[0].equals("0000")){
					regVal1 = regVal2 + regVal3;
				}
				else if(instrParts[0].equals("0010")){
					regVal1 = regVal2 - regVal3;
				}
				else{
					regVal1 = regVal2 * regVal3;
				}
				regVals.put(instrParts[1],regVal1);
				PC+=2;
			}
			else if(instrParts[0].equals("0001") || instrParts[0].equals("0011") || instrParts[0].equals("0111")){
				int regVal1;
				int regVal2 = regVals.get(instrParts[2]);
				int regVal3 = Integer.parseInt(instrParts[3],2);
				if(regVal3>=8){	//DOUBT
					regVal3-=16;
				}
				if(instrParts[0].equals("0001")){
					regVal1 = regVal2 + regVal3;
				}
				else if(instrParts[0].equals("0011")){
					regVal1 = regVal2 - regVal3;
				}
				else{
					regVal1 = regVal2 * regVal3;
				}
				regVals.put(instrParts[1],regVal1);
				PC+=2;
			}
			else if(instrParts[0].equals("1000")){
				int regVal2 = regVals.get(instrParts[2]);
				int regVal3 = regVals.get(instrParts[3]);
				int mem = regVal2+regVal3;
				String memVal = memoryVals.get(String.format("%04d", (mem)))+" "+memoryVals.get(String.format("%04d", (mem+1)));
				memVal = memVal.replace(" ","");
				int regVal1 = Integer.parseInt(memVal,2); 
				if(regVal1>=32768){
					regVal1 = regVal1 - 65536;
				}
				regVals.put(instrParts[1],regVal1);
				PC+=2;
			}
			else if(instrParts[0].equals("1010")){
				int intVal = regVals.get(instrParts[3]);
				int mem = regVals.get(instrParts[1])+regVals.get(instrParts[2]);
				String binVal = intTo16BitStr(intVal);
				memoryVals.put(String.format("%04d", mem), binVal.substring(0,4)+" "+binVal.substring(4,8));
				//System.out.println(String.format("%04d", mem)+" : "+ binVal.substring(0,4)+" "+binVal.substring(4,8));
				memoryVals.put(String.format("%04d", mem+1), binVal.substring(8,12)+" "+binVal.substring(12,16));
				//System.out.println(String.format("%04d", mem+1)+" : "+ binVal.substring(8,12)+" "+binVal.substring(12,16));
				PC+=2;
			}
		}

		writer = new PrintWriter("output.txt", "UTF-8");
		for (int i = 0; i < 1024; i++){
			writer.println(String.format("%04d", i) +" : "+ memoryVals.get(String.format("%04d", i)));
		}
		writer.close();
		file.delete();
	}
}