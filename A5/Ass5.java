import java.io.*;
import java.util.*;

public class Ass5{

	public static String intTo16BitStr(int n){
		String bin = Integer.toBinaryString(n);
		if(bin.length() > 16){
			bin = bin.substring(bin.length() - 16);
		}
		else{
			while(bin.length()<16){
				bin = "0"+bin;
			}
		}
		////System.out.println(bin);
		return bin;
	}
	
	public static Map<String, String> regCodes = new HashMap<String, String>();
	public static Map<String, String> opCodes = new HashMap<String, String>();
	public static Map<String, Integer> regVals = new HashMap<String, Integer>();
	public static Map<String, String> memoryVals = new HashMap<String, String>();
	public static Map<String, Integer> labelOffsets = new HashMap<String, Integer>();
	public static HashMap<String, ArrayList<Integer>> regDependencies = new HashMap<String, ArrayList<Integer>>();
	public static HashMap<String, ArrayList<Integer>> regForwarding = new HashMap<String, ArrayList<Integer>>();

	public static ArrayList<InstructionState> IFOutBuffer = new ArrayList<InstructionState>();
	public static ArrayList<InstructionState> DEOutBuffer = new ArrayList<InstructionState>();
	public static HashMap<Integer, InstructionState> EXOutBuffer = new HashMap<Integer, InstructionState>();
	public static ArrayList<InstructionState> MEMOutBuffer = new ArrayList<InstructionState>();

	public static int memExecCounter = 2;
	public static boolean haltForJMP = false;
	public static boolean rcvdHLT = false;
	public static int PC = 0;

	public static void IF(int PC){
		if(haltForJMP == true || rcvdHLT == true){
			while(IFOutBuffer.size() > 0){
				IFOutBuffer.remove(0);
			}
			return;
		}

		String instr1 = memoryVals.get(String.format("%04d", PC));
		instr1 += " "+memoryVals.get(String.format("%04d", PC+1));
		int NAR1 = PC+2;
		InstructionState InstrStateObj1 = new InstructionState(instr1,NAR1);

		PC+=2;

		String instr2 = memoryVals.get(String.format("%04d", PC));
		instr2 += " "+memoryVals.get(String.format("%04d", PC+1));
		int NAR2 = PC+2;
		InstructionState InstrStateObj2 = new InstructionState(instr2,NAR2);

		IFOutBuffer.add(InstrStateObj1);
		IFOutBuffer.add(InstrStateObj2);

		// System.out.println("IF : "+InstrStateObj1.NAR);
		// System.out.println("IF : "+InstrStateObj2.NAR);
	}

	public static void DE(){
		if(haltForJMP == true || rcvdHLT == true){
			return;
		}

		InstructionState InstrStateObj2 = IFOutBuffer.remove(1); 
		InstructionState InstrStateObj1 = IFOutBuffer.remove(0);

		//System.out.println("DE : "+InstrStateObj1.NAR);
		
		DEWrapper(InstrStateObj1);
		DEOutBuffer.add(InstrStateObj1);

		if(InstrStateObj1.isHLT == true || InstrStateObj1.isJMP == true){
			return;
		}

		//System.out.println("DE : "+InstrStateObj2.NAR);

		DEWrapper(InstrStateObj2);		
		DEOutBuffer.add(InstrStateObj2);
	}

	public static void DEWrapper(InstructionState InstrStateObj){
		String[] instrParts = InstrStateObj.instruction.split("\\s");
		String B1 = instrParts[3];
		String B2 = instrParts[2];
		String IMM = instrParts[0]+" "+instrParts[1];
		InstrStateObj.B1 = B1;
		InstrStateObj.B2 = B2;
		InstrStateObj.IMM = IMM;

		if(instrParts[0].equals("0000") || instrParts[0].equals("0010") || instrParts[0].equals("0110")){
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(InstrStateObj.NAR);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}
		if(instrParts[0].equals("0001") || instrParts[0].equals("0011") || instrParts[0].equals("0111")){
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(InstrStateObj.NAR);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}
		if(instrParts[0].equals("1000")){ // Load
			ArrayList<Integer> RegDep = regDependencies.get(instrParts[1]);
			RegDep.add(InstrStateObj.NAR);
			regDependencies.put(instrParts[1],RegDep);
			//System.out.println("RegDep Added - "+instrParts[1]+" - "+RegDep.toString());
		}

		if(instrParts[0].equals("1100") || instrParts[0].equals("1101")){
			InstrStateObj.isJMP = true;
			haltForJMP = true;
		}

		if(instrParts[0].equals("1110")){ //HLT
			InstrStateObj.isHLT = true;
			rcvdHLT = true;
		}
	}

	public static void CheckDependencies(InstructionState InstrStateObj){
		String[] instrParts = InstrStateObj.instruction.split("\\s");
		String B1 = instrParts[3];
		String B2 = instrParts[2];

		if(instrParts[0].equals("1110")){ //HLT
			InstrStateObj.isHLT = true;
			return;
		}

		boolean isRAW = false;
		if(instrParts[0].equals("1101")){ // BEQZ
			isRAW = checkRAWDependencies(instrParts[1], InstrStateObj.NAR);
		}
		else if(instrParts[0].equals("0000") || instrParts[0].equals("0010") || instrParts[0].equals("0110")){
			isRAW = checkRAWDependencies(B1, InstrStateObj.NAR) || checkRAWDependencies(B2, InstrStateObj.NAR);
		}
		else if(instrParts[0].equals("0001") || instrParts[0].equals("0011") || instrParts[0].equals("0111")){
			isRAW = checkRAWDependencies(B2, InstrStateObj.NAR);
		}
		else if(instrParts[0].equals("1000")){ // Load
			isRAW = checkRAWDependencies(B1, InstrStateObj.NAR) || checkRAWDependencies(B2, InstrStateObj.NAR);	
		}
		else if(instrParts[0].equals("1010")){ // Store
			isRAW = checkRAWDependencies(instrParts[1], InstrStateObj.NAR) || checkRAWDependencies(B1, InstrStateObj.NAR) || checkRAWDependencies(B2, InstrStateObj.NAR);
		}

		InstrStateObj.isRAW = isRAW;

		if(isRAW){
			//System.out.println("isRAW : "+InstrStateObj.NAR+" ----- "+InstrStateObj.isRAW);
			return;
		}

		if(instrParts[0].equals("1100") || instrParts[0].equals("1101")){
			InstrStateObj.isJMP = true;
		}
	}

	public static boolean checkRAWDependencies(String reg, int myPC){
		ArrayList<Integer> regUsage = regDependencies.get(reg);
		//System.out.println(reg+" - "+regUsage.toString());
		if(regUsage.size() == 0){
			return false;
		}
		if(regUsage.contains(myPC) && regUsage.size() > 1){
			if(regUsage.indexOf(myPC) == 0){
				return false;
			}
			for(int i=0;i<regUsage.size();i++){
				int depPC = regUsage.get(i);
				if(depPC < myPC){
					return true;
				}
			}
			return false;
		}
		else if(regUsage.contains(myPC) && regUsage.size() == 1){
			return false;
		}
		else if(!regUsage.contains(myPC) && regUsage.size() > 0){
			for(int i=0;i<regUsage.size();i++){
				int depPC = regUsage.get(i);
				if(depPC < myPC){
					return true;
				}
			}
			return false;
		}

		//boolean isDep = regUsage.contains(myPC);
		// if(isDep){
		// 	ArrayList<Integer> regFwd = regForwarding.get(reg);
		// 	if(regFwd.size() != 0){
		// 		return false;
		// 	} 
		// }
		return false;
	}

	public static InstructionState DEOutBufferPOP(){
		int buffLen = DEOutBuffer.size();
		if(buffLen == 0){
			return null;
		}
		int rmInd = -1;
		for(int i=0;i<buffLen;i++){
			InstructionState InstrStateObj = DEOutBuffer.get(i);
			CheckDependencies(InstrStateObj);
			if(InstrStateObj.isRAW == false){
				if(InstrStateObj.isJMP == true ){
					if(buffLen == 1){
						rmInd = i;
						break;
					}
				}
				else{
					rmInd = i;
					break;
				}
			}
		}
		if(rmInd == -1){
			return null;
		}
		InstructionState InstrStateObj = DEOutBuffer.remove(rmInd);
		return InstrStateObj;
	}

	public static void EX(){
		int buffLen = DEOutBuffer.size();
		if(buffLen == 0){
			return;
		}
		InstructionState InstrStateObj1 = DEOutBufferPOP();
		if(InstrStateObj1 == null){
			return;
		}
		else{
			//System.out.println("EX : "+InstrStateObj1.NAR);
			EXWrapper(InstrStateObj1);
			EXOutBuffer.put(InstrStateObj1.NAR, InstrStateObj1);
		}
		InstructionState InstrStateObj2 = DEOutBufferPOP();
		if(InstrStateObj2 == null){
			return;
		}
		if(InstrStateObj2.isJMP == true){
			DEOutBuffer.add(InstrStateObj2);
			return;
		}
		if(InstrStateObj2 != null){
			//System.out.println("EX : "+InstrStateObj2.NAR);
			EXWrapper(InstrStateObj2);
			EXOutBuffer.put(InstrStateObj2.NAR, InstrStateObj2);
		}
	}

	public static void EXWrapper(InstructionState InstrStateObj){
		String[] instrSubParts = InstrStateObj.IMM.split("\\s");
		String B1 = InstrStateObj.B1;
		String B2 = InstrStateObj.B2;
		String Operation = instrSubParts[0];
		String Reg = instrSubParts[1];
		int B3 = -1;
		if(Operation.equals("0000") || Operation.equals("0010") || Operation.equals("0110")){
			int regVal1;
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = getRegWithFwd(B1);
			if(Operation.equals("0000")){
				regVal1 = regVal2 + regVal3;
			}
			else if(Operation.equals("0010")){
				regVal1 = regVal2 - regVal3;
			}
			else{
				regVal1 = regVal2 * regVal3;
			}
			B3 = regVal1;
			// ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			// RegFwd.add(B3);
			// regForwarding.put(Reg,RegFwd);
			//System.out.println("RegFwd Added - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("0001") || Operation.equals("0011") || Operation.equals("0111")){
			int regVal1;
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = Integer.parseInt(B1,2);
			if(regVal3>=8){
				regVal3-=16;
			}
			if(Operation.equals("0001")){
				regVal1 = regVal2 + regVal3;
			}
			else if(Operation.equals("0011")){
				regVal1 = regVal2 - regVal3;
			}
			else{
				regVal1 = regVal2 * regVal3;
			}
			B3 = regVal1;
			// ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			// RegFwd.add(B3);
			// regForwarding.put(Reg,RegFwd);
			//System.out.println("RegFwd Added - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("1000")){ //Load
			int regVal2 = getRegWithFwd(B2);
			int regVal3 = getRegWithFwd(B1);
			int mem = regVal2+regVal3;
			B3 = mem;
		}
		else if(Operation.equals("1010")){ //Store
			int mem = getRegWithFwd(instrSubParts[1])+getRegWithFwd(B2);
			B3 = mem;
		}
		InstrStateObj.B3 = B3;
		InstrStateObj.Reg = Reg;
		InstrStateObj.Operation = Operation;
	}

	public static boolean MEM(){
		int memPC = memExecCounter;
		boolean isJMP = false;
		if(EXOutBuffer.containsKey(memPC)){
			InstructionState InstrStateObj1 = EXOutBuffer.get(memPC);
			//System.out.println("MEM : "+InstrStateObj1.NAR);
			EXOutBuffer.remove(memPC);
			memPC+=2;
			MEMWrapper(InstrStateObj1);
			if(InstrStateObj1.isJMP == true){
				PC = InstrStateObj1.NPC;
				memExecCounter = InstrStateObj1.NPC + 2;
				isJMP = true;
				haltForJMP = false;
			}
			else{
				MEMOutBuffer.add(InstrStateObj1);
			}
		}
		if(EXOutBuffer.containsKey(memPC)){
			InstructionState InstrStateObj2 = EXOutBuffer.get(memPC);
			//System.out.println("MEM : "+InstrStateObj2.NAR);
			EXOutBuffer.remove(memPC);
			memPC+=2;
			MEMWrapper(InstrStateObj2);
			if(InstrStateObj2.isJMP == true){
				PC = InstrStateObj2.NPC;
				memExecCounter = InstrStateObj2.NPC + 2;
				isJMP = true;
				haltForJMP = false;
			}
			else{
				MEMOutBuffer.add(InstrStateObj2);
			}
		}
		if(isJMP == false){
			memExecCounter = memPC;
		}

		return isJMP;
	}

	public static void MEMWrapper(InstructionState InstrStateObj){
		String Operation = InstrStateObj.Operation;
		int DMAR = InstrStateObj.B3;
		int NAR = InstrStateObj.NAR;
		int MDR = -1;
		int PC = NAR;
		if(Operation.equals("1000")){ //Load
			String memVal = memoryVals.get(String.format("%04d", (DMAR)))+" "+memoryVals.get(String.format("%04d", (DMAR+1)));
			memVal = memVal.replace(" ","");
			MDR = Integer.parseInt(memVal,2);
			if(MDR>=32768){
				MDR = MDR - 65536;
			}
		}
		else if(Operation.equals("1010")){ //Store
			MDR = regVals.get(InstrStateObj.B1);
			String binMDR = intTo16BitStr(MDR);
			memoryVals.put(String.format("%04d", DMAR), binMDR.substring(0,4)+" "+binMDR.substring(4,8));
			memoryVals.put(String.format("%04d", DMAR+1), binMDR.substring(8,12)+" "+binMDR.substring(12,16));
		}
		else if(Operation.equals("1100")){ // JMP
			String offBin = InstrStateObj.Reg + InstrStateObj.B2;
			int off = Integer.parseInt(offBin,2);
			if(off>=128){
                off-=256;
            }
            PC = NAR-2+off;
		}
		else if(Operation.equals("1101")){ // BEQZ
            int regVal = regVals.get(InstrStateObj.Reg);
            if(regVal == 0){
            	String offBin = InstrStateObj.B2 + InstrStateObj.B1;
				int off = Integer.parseInt(offBin,2);
				if(off>=128){
                    off-=256;
                }
                PC = NAR-2+off;
            }
		}
		InstrStateObj.MDR = MDR;
		InstrStateObj.NPC = PC;
	}

	public static boolean SR(){
		boolean isHLT = false;
		while(MEMOutBuffer.size() > 0){
			InstructionState InstrStateObj = MEMOutBuffer.remove(0);
			SRWrapper(InstrStateObj);
			if(InstrStateObj.isHLT == true){
				isHLT = true;
			}
			//System.out.println("SR : "+InstrStateObj.NAR+" ------- "+isHLT);
		}
		return isHLT;
	}

	public static void SRWrapper(InstructionState InstrStateObj){
		String Operation = InstrStateObj.Operation;
		int B3 = InstrStateObj.B3;
		int MDR = InstrStateObj.MDR;
		String Reg = InstrStateObj.Reg;
		if(Operation.equals("0000") || Operation.equals("0010") || Operation.equals("0110") ||
			Operation.equals("0001") || Operation.equals("0011") || Operation.equals("0111")){
			regVals.put(Reg,B3);

			ArrayList<Integer> RegDep = regDependencies.get(Reg);
			int rmInd = RegDep.indexOf(InstrStateObj.NAR);
			RegDep.remove(rmInd);
			regDependencies.put(Reg,RegDep);
			//System.out.println("Removed Dep - "+Reg+" - "+RegDep.toString());

			// ArrayList<Integer> RegFwd = regForwarding.get(Reg);
			// RegFwd.remove(0);
			// regForwarding.put(Reg,RegFwd);
			// System.out.println("Removed Fwd - "+Reg+" - "+RegFwd.toString());
		}
		else if(Operation.equals("1000")){ //Load
			regVals.put(Reg,MDR);

			ArrayList<Integer> RegDep = regDependencies.get(Reg);
			int rmInd = RegDep.indexOf(InstrStateObj.NAR);
			RegDep.remove(rmInd);
			regDependencies.put(Reg,RegDep);
		}
		//System.out.println("SR Fin - "+InstrStateObj.NAR);
	}

	public static int getRegWithFwd(String reg){
		// ArrayList<Integer> regFwd = regForwarding.get(reg);
		// if(regFwd.size() > 0){
		// 	return regFwd.get(0);
		// }
		return regVals.get(reg);
	}

	public static void main(String[] args) throws IOException {

		for(int n=0;n<16;n++){
			String code = Integer.toBinaryString(n);
			while(code.length()<4){
				code = "0"+code;
			}
			regCodes.put("R"+Integer.toString(n),code);

			ArrayList<Integer> depCodes = new ArrayList<Integer>();
			regDependencies.put(code,depCodes);
			ArrayList<Integer> fwdVals = new ArrayList<Integer>();
			regForwarding.put(code,fwdVals);
		}

		opCodes.put("ADD" ,"0000");
		opCodes.put("SUB" ,"0010");
		opCodes.put("MUL" ,"0110");
		opCodes.put("LD"  ,"1000");
		opCodes.put("SD"  ,"1010");
		opCodes.put("JMP" ,"1100");
		opCodes.put("BEQZ","1101");
		opCodes.put("HLT" ,"1110");
		opCodes.put("ADDIM" ,"0001");
		opCodes.put("SUBIM" ,"0011");
		opCodes.put("MULIM" ,"0111");
		
		for(int i = 0; i < 16; i++) {
			regVals.put(regCodes.get("R"+Integer.toString(i)), 0);
		}
		
		for(int i = 0; i < 1024; i++) {
			memoryVals.put(String.format("%04d", i), "0000 0000");
		}

		PrintWriter writer = new PrintWriter("input.txt", "UTF-8");
		Scanner scan = new Scanner(System.in);
		String line = scan.nextLine();
		while(!(line.contains("HLT"))){
			writer.println(line);
			line = scan.nextLine();
		}
		writer.println(line);
		writer.close();

		File file = new File("input.txt");
		BufferedReader b = new BufferedReader(new FileReader(file));
		line=null;
		int hashes = 0;
		int instrNum = 0;
		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			if(hashes == 3){
				if(line.contains(":")){
					String label = (line.split(":"))[0];
					labelOffsets.put(label,instrNum);
				}
				instrNum++;
			}
		}

		b = new BufferedReader(new FileReader(file));
		line = null;
		hashes = 0;

		while ((line = b.readLine()) != null){
			if(line.contains("##")){
				hashes++;
				continue;
			}
			if(hashes == 1){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					System.out.println("Invalid Input");
					System.exit(0);
				}
				regVals.put(regCodes.get(parts[1].trim()),Integer.parseInt(parts[2].trim()));
			}
			else if(hashes == 2){
				String[] parts = line.split("\\$");
				if(parts.length != 3){
					System.out.println("Invalid Input");
					System.exit(0);
				}
				int mem = Integer.parseInt(parts[1].trim());
				String binVal = intTo16BitStr(Integer.parseInt(parts[2].trim()));
				memoryVals.put(String.format("%04d", mem), binVal.substring(0,4)+" "+binVal.substring(4,8));
				memoryVals.put(String.format("%04d", mem+1), binVal.substring(8,12)+" "+binVal.substring(12,16));
			}
			else{
				//System.out.println(PC+" - "+line);
				String[] parts = line.split("\\s");
				for(int k=0;k<parts.length;k++){
					parts[k] = parts[k].trim();
				}
				if(parts[0].contains(":")){
					List<String> list = new ArrayList<String>(Arrays.asList(parts));
					list.removeAll(Arrays.asList(parts[0]));
					parts = list.toArray(new String[list.size()]);
				}
				if(parts[0].equals("ADD") || parts[0].equals("SUB") || parts[0].equals("MUL")){
					String binInstr;
					if(parts[3].contains("#")){
						parts[0]+="IM";
					}
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					if(parts[3].contains("#")){
						String bin = Integer.toBinaryString(Integer.parseInt(parts[3].replace("#","")));
						if(bin.length()>4){
							bin = bin.substring(bin.length()-4);
						}
						else{
							while(bin.length()<4){
								bin = "0"+bin;
							}
						}
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+bin);
					}
					else{
						memoryVals.put(String.format("%04d", PC), regCodes.get(parts[2])+" "+regCodes.get(parts[3]));
					}
				}
				else if(parts[0].equals("LD")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(parts[1]));
					PC++;
					String reg1 = (parts[2].split("\\["))[0];
					String reg2 = (((parts[2].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg1)+" "+regCodes.get(reg2));
				}
				else if(parts[0].equals("SD")){
					String reg1 = (parts[1].split("\\["))[0];
					String reg2 = (((parts[1].split("\\["))[1]).split("\\]"))[0];
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg1));
					PC++;
					memoryVals.put(String.format("%04d", PC), regCodes.get(reg2)+" "+regCodes.get(parts[2]));
				}
				else if(parts[0].equals("BEQZ")){
					int offset = labelOffsets.get(parts[2]);
					offset=(2*offset)-PC;
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					String reg = (parts[1].replace("(","")).replace(")","");
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+regCodes.get(reg));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(0,4)+" "+offsetBin.substring(4,8));
				}
				else if(parts[0].equals("JMP")){
					int offset = labelOffsets.get(parts[1]);
					offset=(2*offset)-PC;
                    String offsetBin = Integer.toBinaryString(offset);
                    if(offsetBin.length()>8){
                        offsetBin = offsetBin.substring(offsetBin.length()-8);
                    }
                    else{
                        while(offsetBin.length()<8){
                            offsetBin = "0" + offsetBin;
                        }
                    }
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" "+offsetBin.substring(0,4));
					PC++;
					memoryVals.put(String.format("%04d", PC), offsetBin.substring(4,8)+" 0000");
				}
				else if(parts[0].equals("HLT")){
					memoryVals.put(String.format("%04d", PC), opCodes.get(parts[0])+" 0000");
                    PC++;
				}
				PC++;
			}
		}
		//System.out.println();
		
		HashMap<String,InstructionState> Pipieline = new HashMap<String,InstructionState>();

		boolean stallPipeline = false;
		boolean isRAWPipeline = false;

		PC = 0;

		int iter = 1;

		while(PC < 512){
			boolean isHLT = false;
			boolean isJMP = false;

			if(iter == 0){
				isHLT = SR();
				if(isHLT == true){
					break;
				}
				IF(PC);
				iter = 1;
			}
			else if(iter == 1){
				IF(PC);
			}
			else if(iter == 2){
				DE();
				IF(PC);
			}
			else if(iter == 3){
				EX();
				DE();
				IF(PC);
			}
			else if(iter == 4){
				isJMP = MEM();
				if(isJMP == false){
					EX();
					DE();
					IF(PC);
				}
			}
			else{
				isHLT = SR();
				if(isHLT == true){
					break;
				}
				isJMP = MEM();
				if(isJMP == false){
					EX();
					DE();
					IF(PC);
				}
			}

			//System.out.println(PC);

			if(isJMP == true){
				iter = 0;
			}
			else{
				PC+=4;
				iter+=1;
			}	
			
			if(PC > 80){
				break;
			}
		}
		
		writer = new PrintWriter("output.txt", "UTF-8");
		for (int i = 0; i < 1024; i++){
			writer.println(String.format("%04d", i) +" : "+ memoryVals.get(String.format("%04d", i)));
		}
		writer.close();
		file.delete();
	}
}

class InstructionState {
	
	String instruction;
	int NAR;

	String B1;
	String B2;
	String IMM;

	int B3;
	String Reg;
	String Operation;

	int MDR;
	int NPC;

	boolean isJMP;
	boolean isRAW;
	boolean isHLT;

	InstructionState(String instruction, int NAR){
		this.instruction = instruction;
		this.NAR = NAR;
		this.isJMP = false;
		this.isRAW = false;
		this.isHLT = false;
	}
}